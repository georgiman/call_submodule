resource "null_resource" "echo_dummyy" {
  depends_on = [module.randompet]
  provisioner local-exec {
    command = "ls -la"
  }
 
  triggers = {
     hack = "${timestamp()}"
  }
}

module "randompet" {
source = "git::ssh://git@bitbucket.org/georgiman/submodule.git//folder/randompet?ref=0.0.7"
}

resource "null_resource" "echo_dummyy1" {
  depends_on = [module.randompet]
  provisioner local-exec {
    command = "find | sed 's|[^/]*/|- |g'"
  }
 
  triggers = {
     hack = "${timestamp()}"
  }
}

resource "null_resource" "echo_dummyy2" {
  provisioner local-exec {
    command = "pwd"
  }
 
  triggers = {
     hack = "${timestamp()}"
  }
}

resource "null_resource" "echo_dummyy3" {
  depends_on = [module.randompet]
  provisioner local-exec {
    command = "cat ${path.module}/lambda-code/code.txt"
  }
 
  triggers = {
     hack = "${timestamp()}"
  }
}